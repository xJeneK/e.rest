const koa = require('koa');
const app = koa();
const Route = require('./lib/e.router.js')();
const koaBody = require('koa-body');
const cors = require('./lib/e.cors');

const users = require('./routes/users');

app.use(koaBody());

Route.get('/', function *() {
    this.body = '/';
});

Route.head('/', function *() {
    this.set('test','true');
});

Route.get('/user/:id/:photo', function *() {
    this.body = this.params;
});

Route.add('/users/', users);

app.use(cors());
app.use(Route.R());
app.listen(3000);