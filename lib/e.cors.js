module.exports = function (Origin = '*', Methods = 'GET, POST, PUT, DELETE, HEAD', Headers = 'Origin, X-Requested-With, Content-Type, Accept') {
    return function *(next) {
        this.set('Access-Control-Allow-Origin', Origin);
        this.set('Access-Control-Allow-Methods', Methods);
        this.set('Access-Control-Allow-Headers', Headers);
        yield next;
    }
};