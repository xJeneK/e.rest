const routes = require('../lib/e.router')();

routes.get('/', function *() {
    this.body = '/users/';
});

routes.get('/:id', function *() {
    this.body = '/users/' + this.params.id;
});

module.exports = routes;